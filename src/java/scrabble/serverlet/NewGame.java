/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrabble.serverlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import scrabble.utils.ServletUtils;
import scrabble.utils.SessionUtils;
import ws.scrabble.DuplicateGameName_Exception;
import ws.scrabble.GameDoesNotExists_Exception;
import ws.scrabble.InvalidParameters_Exception;
import ws.scrabble.ScrabbleWebService;

/**
 *
 * @author danielo
 */
@WebServlet(name = "NewGame", urlPatterns = {"/NewGame"})
public class NewGame extends HttpServlet {
    
    
    

    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             
        String playerName = request.getParameter("playername");
        String computerPlayers = request.getParameter("computerplayers");
        String humanPlayers = request.getParameter("humanplayers");
        String gameName = request.getParameter("gamename");
        
        ScrabbleWebService scrabbleService = ServletUtils.getScrabbleService(getServletContext());
        try {
            scrabbleService.createGame(gameName, Integer.parseInt(humanPlayers), Integer.parseInt(computerPlayers));
            int id = scrabbleService.joinGame(gameName, playerName);
            
            SessionUtils.setPlayerGameName(request, gameName);
            SessionUtils.setPlayerID(request, id);
            SessionUtils.setPlayerCurrentEvent(request, 0);
            
            response.sendRedirect("waitforplayers.html");
        } catch(DuplicateGameName_Exception | InvalidParameters_Exception | GameDoesNotExists_Exception ex) {
            System.err.println(ex.getMessage());
            SessionUtils.setError(request, ex.getMessage());
            getServletContext().getRequestDispatcher("/newgame.jsp").forward(request, response);
        } catch(NumberFormatException ex) {
            System.err.println(ex.getMessage());
            SessionUtils.setError(request, "ERROR " + ex.getMessage());
            getServletContext().getRequestDispatcher("/newgame.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
