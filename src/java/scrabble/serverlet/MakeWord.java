/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrabble.serverlet;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import scrabble.utils.ServletUtils;
import scrabble.utils.SessionUtils;
import ws.scrabble.InvalidParameters_Exception;
import ws.scrabble.Orientation;
import ws.scrabble.ScrabbleWebService;

/**
 *
 * @author danielo
 */
@WebServlet(name = "MakeWord", urlPatterns = {"/makeWord"})
public class MakeWord extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            String body = parsRequest(request);
            System.out.println(body);
            
            Gson gson = new Gson();
            SubmitData submitData = gson.fromJson(body, SubmitData.class);
            System.out.println(submitData.lettersStartPos);
            System.out.println(submitData.orientation);
            System.out.println(submitData.wordStartPos);
            System.out.println(submitData.letters);
            System.out.println(submitData.word);
            
            
            ScrabbleWebService scrabbleService = ServletUtils.getScrabbleService(getServletContext());

            int playerId = SessionUtils.getPlayerID(request);
            int currentEventId = SessionUtils.getPlayerCurrentEvent(request);
            
            Orientation orientation;
            if(submitData.orientation.equalsIgnoreCase("HORIZONTAL"))
                orientation = Orientation.HORIZONTAL;
            else
                orientation = Orientation.VERTICAL;
            
            scrabbleService.makeWord(
                    playerId, 
                    currentEventId, 
                    submitData.lettersStartPos, 
                    orientation, 
                    submitData.letters,
                    submitData.wordStartPos, 
                    orientation,
                    submitData.word);
            
        } catch(InvalidParameters_Exception ex) {
            System.err.println(ex.getMessage());
            out.println(ex.getMessage());
        }
    }
    
    private static class SubmitData {
        public String lettersStartPos;
        public String orientation;
        public String letters;
        public String wordStartPos;
        public String word;
        
        public SubmitData() {}
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
    private String parsRequest(HttpServletRequest request) throws IOException {
        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }
        body = stringBuilder.toString();
        return body;
    }
}
