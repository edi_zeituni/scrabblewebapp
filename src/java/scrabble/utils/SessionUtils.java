package scrabble.utils;

import scrabble.constants.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author blecherl
 */
public class SessionUtils {
    private static final String PLAYER_ID = "playerId";
    private static final String EVENT_ID = "eventId";
    private static final String GAME_NAME = "gamename";
    private static final String ERROR_MSG = "errorMsg";
    
    public static void setPlayerGameName(HttpServletRequest request, String gameName) {
        HttpSession session = request.getSession(true);
        session.setAttribute(GAME_NAME, gameName);
    }
    
    public static String getPlayerGameName(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String sessionAttribute = session != null ? (String)session.getAttribute(GAME_NAME) : null;
        return sessionAttribute != null ?  sessionAttribute : null;
    }
    
    public static void setPlayerID(HttpServletRequest request, Integer id) {
        HttpSession session = request.getSession(true);
        session.setAttribute(PLAYER_ID, id);
    }
    
    public static int getPlayerID (HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Integer sessionAttribute = session != null ? (Integer)session.getAttribute(PLAYER_ID) : null;
        return sessionAttribute != null ?  sessionAttribute : null;
    }
    
    public static int getPlayerCurrentEvent (HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Integer sessionAttribute = session != null ? (Integer)session.getAttribute(EVENT_ID) : null;
        return sessionAttribute != null ?  sessionAttribute : null;
    }
    
    public static void setPlayerCurrentEvent(HttpServletRequest request, Integer id) {
        HttpSession session = request.getSession(true);
        session.setAttribute(EVENT_ID, id);
    }    
    
    public static void setError(HttpServletRequest request, String error) {
        HttpSession session = request.getSession();
        session.setAttribute(ERROR_MSG, error);
    }
    
    public static String getError(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String sessionAttribute = session != null ? (String)session.getAttribute(ERROR_MSG) : null;
        return sessionAttribute != null ?  sessionAttribute : null;
    }

    public static void clearSession (HttpServletRequest request) {
        request.getSession().invalidate();
    }
}