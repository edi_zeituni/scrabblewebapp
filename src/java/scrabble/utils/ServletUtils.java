package scrabble.utils;

import static scrabble.constants.Constants.INT_PARAMETER_ERROR;
import java.net.MalformedURLException;
import java.net.URL;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import ws.scrabble.ScrabbleWebService;
import ws.scrabble.ScrabbleWebServiceService;

/**
 *
 * @author blecherl
 */
public class ServletUtils {
    private static final String PORT = "8080";
    private static final String ADDRESS = "127.0.0.1";
    private static final String SCRABBLE_WEB_SERVICE = "scrabbleService";
    
    public static ScrabbleWebService getScrabbleService (ServletContext servletContext) {
        if (servletContext.getAttribute(SCRABBLE_WEB_SERVICE) == null) {
            servletContext.setAttribute(SCRABBLE_WEB_SERVICE, createNewScrabbleService());
        }
        return (ScrabbleWebService) servletContext.getAttribute(SCRABBLE_WEB_SERVICE);
    }

    private static ScrabbleWebService createNewScrabbleService() {
        ScrabbleWebServiceService service;
        try {
            URL location = new URL("http://" + ADDRESS + ":" + PORT + "/scrabble/ScrabbleWebServiceService?wsdl");
        
            service = new ScrabbleWebServiceService(location);
            return service.getScrabbleWebServicePort();
        } catch(MalformedURLException ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
}