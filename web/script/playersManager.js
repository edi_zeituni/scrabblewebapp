var app = angular.module('playersManager',[]);


app.factory('PollPlayers', function($http) {
    var GET_PLAYERS_URL = 'getPlayersDetails';
    var GET_CLIENT_PLAYER_DETAILS_URL = 'getPlayerDetails';
    var PASS_TURN_URL = 'passTurn';
    var REAPLACE_LETTERS = 'replaceLetters';
    var players = [];
    var shouldLockUI = {status : "yes"};
    var emptyCell = 'cardBackground';
    var clientCards = [];
    var changedCardsList = [];
    var bucket = [];
    var onDropBucket = function(data,evt)
    {
        bucket.push(clientPlayer.cards[data.index].dragedValue);
        this.value= 'bucket';
    };
    var exchangeBucket = {bucket: bucket , value: 'emptyBucket' , onDrop: onDropBucket, hide: 'true' };
    var onDragHandle = function(data,evt)
    {
        changedCardsList.push({card:this , origVal:this.value});
        this.value= emptyCell;
        this.dropable= "true";
        this.dragable = "false";
    };
    for (var i = 0, max = 7; i < max; i++) {
        clientCards[i] = {value: 'cardBackground', dragedValue: 'cardBackground', index:i, dropable: "false",dragable: "false", onDrag: onDragHandle};
    }
    var clientPlayer = {name: "TODO" , cards : clientCards};

    var pollPlayers = function() {
      $http.get(GET_PLAYERS_URL).then(function(r) {
        var playersResult = r.data;
        players.length = 0;
        playersResult.forEach(function(entry) {
              var player = {};
              player.name = entry.name;
              player.score = entry.score;
              player.type = entry.type;
              player.background = "white";
              players.push(player);
        });
      });

    };
    
    var replaceLetters = function replaceLetters(letters) {
        console.log("In replace!!!" + letters);    
        $http.post(REAPLACE_LETTERS, letters).
            success(function(data, status, headers, config) {
                console.log("SUCCESS submiting replace cards");   
            }).
            error(function(data, status, headers, config) {
                console.error("Failed to submit replace cards");
            });
    }
  
    var pollClientPlayerCards = function() {
      changedCardsList.length = 0;
      $http.get(GET_CLIENT_PLAYER_DETAILS_URL).then(function(r) {
        var cardsResult = decodeURIComponent(r.data.letters);
        clientPlayer.name = r.data.name;
        var counter = 0;
        for (var i=0 ; i< cardsResult.length ; i+=2){
            if (cardsResult[i] == ' ')
              clientCards[counter].value = "joker";
            else
              clientCards[counter].value = cardsResult[i];
            clientCards[counter].dragedValue = clientCards[counter].value;
            clientCards[counter].dragable = "true";
            counter++;
        }
        for (var i= counter ; i<7 ; i++){
            clientCards[counter].value = emptyCell;
            clientCards[counter].dragedValue = emptyCell;
            clientCards[counter].dragable = "false";
        }
      })
    };
  
    var passTurn = function passTurn() {
      $http.get(PASS_TURN_URL).then(function(r) {
        //TODO - handle errors
      })
    };
    
    var resignPlayer = function resignPlayer(resignPlayerName) {
        var i = 0;
        var isFound = false;
        for ( ; i < players.length ; i++) {
            if (players[i].name === resignPlayerName) 
            {
                isFound = true;
                break;
            }
        }
        if (isFound == true) {
            players.splice(i, 1);
        }
        if (resignPlayerName === clientPlayer.name) {
            window.location.replace("gametimeout.html");
        }
    }
  
    var updateScoreForPlayer = function updateScoreForPlayer(playerName,score)
    {
        for (var i = 0; i < players.length ; i++) {
            if (players[i].name === playerName) {
                players[i].score += score;
                break;
            }
        }
        if(score > 0 && playerName === clientPlayer.name){
            pollClientPlayerCards();
            return "true";
        }
        
        if(score < 0 && playerName === clientPlayer.name){
            pollClientPlayerCards();
            return "false";
        }
        
        return "";
    };
    
    var currentPlayerHasChanged = function currentPlayerHasChanged(playerName)
    { 
        if(playerName === clientPlayer.name)
            shouldLockUI.status = "no";
        else
             shouldLockUI.status = "yes";
        for (var i = 0; i < players.length ; i++) {
            if (players[i].name === playerName) 
                players[i].background = "orange";
            else 
                players[i].background = "white";
        }
    };
    
    var clearBucket = function clearBucket(){
        bucket.length = 0;
        exchangeBucket.value = 'emptyBucket';
    }
    
    pollPlayers();
    pollClientPlayerCards();
  
    return {
        players: players,
        clientPlayer : clientPlayer,
        shouldLockUI: shouldLockUI,
        pollPlayers: pollPlayers,
        pollClientPlayerCards: pollClientPlayerCards,
        updateScoreForPlayer: updateScoreForPlayer,
        currentPlayerHasChanged: currentPlayerHasChanged,
        passTurn: passTurn,
        resignPlayer: resignPlayer,
        replaceLetters: replaceLetters,
        exchangeBucket: exchangeBucket,
        clearBucket: clearBucket
    };
});

//    
    //
