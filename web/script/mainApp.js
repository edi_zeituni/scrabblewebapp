var myApp = angular.module('mainApp', ['eventsManager','boardManager', 'cardsManager','playersManager','ngDraggable']);
    
myApp.controller('MainController', ['$scope', 'Poller', 'Board', 'Cards','PollPlayers', function($scope,Poller,Board,Cards,PollPlayers) {
  $scope.events = Poller.data;
  $scope.players = PollPlayers.players;
  $scope.board = Board.board;
  //$scope.clientCards = Cards.clientCards;
  $scope.clientCards = PollPlayers.clientPlayer.cards;
  $scope.exchangeBucket = PollPlayers.exchangeBucket;
  var lockUI = {lock:"no"};
  $scope.shouldLock = PollPlayers.shouldLockUI;
  $scope.test = {value:"red"};
  //PollPlayers.pollPlayers();
  $scope.reset = function reset() {
        reloadView();
        $scope.test.value = "blue";
  };
  $scope.pass = function pass() {
        $scope.shouldLock.status = "yes";
        PollPlayers.passTurn();
        reloadView();
  };
  
  $scope.replace = function() {
      if ($scope.exchangeBucket.bucket.length == 0){
        //http://www.srirangan.net/demos/notifier.js/index.html
        Notifier.info('The exchange bucket is empty!');
        return;
      }
      $scope.shouldLock.status = "yes";
      var letters = "";
      for (var i = 0 ; i< $scope.exchangeBucket.bucket.length ; i++ )
      {
        if ($scope.exchangeBucket.bucket[i] == 'joker') 
            letters += ' ';
        else
          letters += $scope.exchangeBucket.bucket[i];
      }
      PollPlayers.clearBucket();
      PollPlayers.replaceLetters(letters);
      reloadView();
  };  
  
  var reloadView = function(){
    PollPlayers.pollClientPlayerCards();
    PollPlayers.clearBucket();
    Board.removeUncommitedCards();
  };
  $scope.submit = function() {
      Board.submit();
  }
}]);

myApp.factory('MainApp', function() {
    
    var animatePass = function animatePass() {
      //TODO
    };
    
    return {
    animatePass: animatePass
  };
  
});
