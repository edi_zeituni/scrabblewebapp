var boardManager = angular.module('boardManager', ['playersManager', 'ngDialog']);
boardManager.controller('BoardController', ['$scope', function($scope) {
    $scope.jokerValue = "hhhhh";  
}]);

boardManager.factory('Board', function($http, PollPlayers,ngDialog) {
    var board = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    var ROW = 15;
    var COL = 15;
    //empty board
    var eb;
    var isBoardEmpty = true;
    var changedCellsList = [];
    var replace = function replace() {
      board[8][8] = 'A';
    };
    
    var changeCells = function changeCells (lettersStartPos, 
                                   lettersOrientation,
                                   letters)
    {
        letters = letters.toUpperCase();
        var col = getColNumFromPosition(lettersStartPos);
        var row = getRowNumFromPosition(lettersStartPos);
        var addToCol = 0;
        var addToRow = 0;
        
        if (lettersOrientation == "HORIZONTAL")
            addToCol = 1;    
        else
            addToRow =1;
        
        for (var i = 0, len = letters.length; i < len; i++) {
            board[row][col].value = letters[i];
            col += addToCol;
            row += addToRow;
        }
        isBoardEmpty = false;
    };
    
    var getColNumFromPosition = function getColNumFromPosition(position){ 
        // charCodeAt used to get the ascii code value of a character
        return position[0].charCodeAt(0) - 'A'.charCodeAt(0);
    }
    
    var getRowNumFromPosition = function getRowNumFromPosition(position){
        var row = position[1] - '1';
        if (position.length === 3 ){
            row = position[1] - '0';
            row *= 10;
            row += position[2] - '1';
        }
        return row;
    }

    eb = [
    //row1
    [
        'tw-cell', 'regular-cell', 'regular-cell', 'dl-cell','regular-cell', 'regular-cell','regular-cell', 'tw-cell', 'regular-cell', 'regular-cell','regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'tw-cell'
    ],
    //row2
    [
        'regular-cell', 'dw-cell', 'regular-cell', 'regular-cell','regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell','dw-cell', 'regular-cell'
    ],
    //row3
    [
        'regular-cell', 'regular-cell', 'dw-cell', 'regular-cell','regular-cell', 'regular-cell','dl-cell', 'regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'regular-cell', 'dw-cell','regular-cell', 'regular-cell'
    ],
    //row4
    [
        'dl-cell', 'regular-cell', 'regular-cell', 'dw-cell','regular-cell', 'regular-cell','regular-cell', 'dl-cell', 'regular-cell', 'regular-cell','regular-cell', 'dw-cell', 'regular-cell','regular-cell', 'dl-cell'
    ],
    //row5
    [
        'regular-cell', 'regular-cell', 'regular-cell', 'regular-cell','dw-cell', 'regular-cell','regular-cell', 'regular-cell', 'regular-cell', 'regular-cell','dw-cell', 'regular-cell', 'regular-cell','regular-cell', 'regular-cell'
    ],
    //row6
    [
        'regular-cell', 'tl-cell', 'regular-cell', 'regular-cell','regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell','tl-cell', 'regular-cell'
    ],
    //row7
    [
        'regular-cell', 'regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'regular-cell','dl-cell', 'regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'regular-cell', 'dl-cell','regular-cell', 'regular-cell'
    ],
    //row8
    [
        'tw-cell', 'regular-cell', 'regular-cell', 'dl-cell','regular-cell', 'regular-cell','regular-cell', 'center-cell', 'regular-cell', 'regular-cell','regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'tw-cell'
    ],
    //row9
    [
        'regular-cell', 'regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'regular-cell','dl-cell', 'regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'regular-cell', 'dl-cell','regular-cell', 'regular-cell'
    ],
    //row10
    [
        'regular-cell', 'tl-cell', 'regular-cell', 'regular-cell','regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell','tl-cell', 'regular-cell'
    ],
    //row11
    [
        'regular-cell', 'regular-cell', 'regular-cell', 'regular-cell','dw-cell', 'regular-cell','regular-cell', 'regular-cell', 'regular-cell', 'regular-cell','dw-cell', 'regular-cell', 'regular-cell','regular-cell', 'regular-cell'
    ],
    //row12
    [
        'dl-cell', 'regular-cell', 'regular-cell', 'dw-cell','regular-cell', 'regular-cell','regular-cell', 'dl-cell', 'regular-cell', 'regular-cell','regular-cell', 'dw-cell', 'regular-cell','regular-cell', 'dl-cell'
    ],
    //row13
    [
        'regular-cell', 'regular-cell', 'dw-cell', 'regular-cell','regular-cell', 'regular-cell','dl-cell', 'regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'regular-cell', 'dw-cell','regular-cell', 'regular-cell'
    ],
    //row14
    [
        'regular-cell', 'dw-cell', 'regular-cell', 'regular-cell','regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell', 'tl-cell','regular-cell', 'regular-cell', 'regular-cell','dw-cell', 'regular-cell'
    ],
    //row15
    [
        'tw-cell', 'regular-cell', 'regular-cell', 'dl-cell','regular-cell', 'regular-cell','regular-cell', 'tw-cell', 'regular-cell', 'regular-cell','regular-cell', 'dl-cell', 'regular-cell','regular-cell', 'tw-cell'
    ]];
    
    var submit = function submit() {
        var orientation = getCellOrientetion();
        console.log(orientation);
        sortCards(orientation);
        console.log(changedCellsList);
        if(validateCardsPlacement(orientation) === "false") {
            Notifier.error("Word is illegal")
            return "false";            
        }
        var wordStartCell = getStartCell(orientation);
        console.log(wordStartCell);
        var word = getWord(orientation, wordStartCell);
        console.log(word);
        var lettersStartPos = getBoardLocationFormat(changedCellsList[0].x, changedCellsList[0].y);
        console.log(lettersStartPos);
        var wordStartPos = getBoardLocationFormat(wordStartCell.x, wordStartCell.y);
        console.log(wordStartPos);
        var letters = getLettersOnBoard();
        console.log(letters);
        submitToServer(lettersStartPos, orientation, letters, wordStartPos, orientation, word);
//        changedCellsList.length = 0;
        return "true";
    };
    
    var isEmpty = function isEmpty(x, y) {
        if(board[y][x].value === eb[y][x])
            return "true";
        
        return "false";
    };
    
    var getBoardLocationFormat = function getBoardLocationFormat(x, y) {
        var location;
        var a = 'A';
        var codeA = a.charCodeAt();
        
        location = String.fromCharCode(codeA + x);
        y++;
        location += y.toString();
        
        return location;
    };
    
    var getCellOrientetion = function() {
        var seenHorizontalOrientation = "false";
        var seenVerticalOrientation = "false";
        
        var currCell = changedCellsList[0];
        
        console.log(changedCellsList.length);
        for(var i=1; i < changedCellsList.length; i++) {
            
            if(currCell.x === changedCellsList[i].x) {
                seenVerticalOrientation = "true";
                if(seenHorizontalOrientation === "true") {
                    console.log("SEEN HORIZONTAL AND VERTICAL ORIENTATION");
                }
            }

            if(currCell.y === changedCellsList[i].y) {
                seenHorizontalOrientation = "true";
                if(seenVerticalOrientation === "true") {
                    console.log("SEEN HORIZONTAL AND VERTICAL ORIENTATION");
                }
            }
            currCell = changedCellsList[i];
        }

        if(seenHorizontalOrientation === "true")
            return "HORIZONTAL";
        if(seenVerticalOrientation === "true")
            return "VERTICAL";
        
        return getOrientationForSingleCardOnBoard();
    };
    
    var getOrientationForSingleCardOnBoard = function() {
        if(changedCellsList.length === 0) 
            return Orientation.HORIZONTAL;
        
        var previousCell;
        var cell = changedCellsList[0];
        var col = cell.x;
        var row = cell.y;
        
        if(col-1 >= 0){
            previousCell = board[row][col-1];
            if(isEmpty(previousCell.x,previousCell.y) === "false")
                return "HORIZONTAL";
        }
        if (row-1 >= 0) {
            previousCell = board[row-1][col];
            if(isEmpty(previousCell.x,previousCell.y) === "false")
                return "VERTICAL";
        }
        if(col+1 <= 14) {
            previousCell = board[row][col+1];
            if(isEmpty(previousCell.x,previousCell.y) === "false")
                return "HORIZONTAL";
        }
        //default orientation 
        return "VERTICAL";
    };
    
    function horizontalCompare(a,b) {
        if (a.x < b.x)
           return -1;
        if (a.x > b.x)
          return 1;
        return 0;
    };
    
    function verticalCompare(a,b) {
        if (a.y < b.y)
           return -1;
        if (a.y > b.y)
          return 1;
        return 0;
    };
    
    var sortCards = function(orientation) {
        if(orientation === "HORIZONTAL") {
            changedCellsList.sort(horizontalCompare);
        }
        if(orientation === "VERTICAL") {
            changedCellsList.sort(verticalCompare);
        }
    };
    
    var validateCardsPlacement = function(orientation) {
        var currCellComp = changedCellsList[0];
        
        if(orientation === "VERTICAL") {
            for(var i = 1; i < changedCellsList.length; i++) {
                var tempCellComp = changedCellsList[i];
                if(currCellComp.y + 1 !== tempCellComp.y || currCellComp.x !== tempCellComp.x) {
                    return "false";
                }
                currCellComp = tempCellComp;
            }
        }
        if(orientation === "HORIZONTAL") {
            for(var i = 1; i < changedCellsList.length; i++) {
                var tempCellComp = changedCellsList[i];
                if(currCellComp.x + 1 !== tempCellComp.x || currCellComp.y !== tempCellComp.y) {
                    return "false";
                }
                currCellComp = tempCellComp;
            }
        }
        return "true";
    };
    
    var getStartCell = function(orientation) {
        var firstCellComponent = changedCellsList[0];
        var cellModel = null;
                        
        if(orientation === "VERTICAL") {
            if(firstCellComponent.y - 1 >= 0) { 
                cellModel = board[firstCellComponent.y - 1][firstCellComponent.x];
                for(var i = cellModel.y; isEmpty(cellModel.x,cellModel.y) === "false" && i >= 0; i--) {
                    var cellModelTemp = board[i][cellModel.x];
                    if(isEmpty(cellModelTemp.x,cellModelTemp.y) === "true"){
                        break;
                    }
                    cellModel = cellModelTemp;
                }
            } else {
                cellModel = board[firstCellComponent.x][firstCellComponent.y];
            }
        }
        if(orientation === "HORIZONTAL") {
            if(firstCellComponent.x - 1 >= 0) { 
                cellModel = board[firstCellComponent.y][firstCellComponent.x - 1];
                for(var i = cellModel.x - 1;  isEmpty(cellModel.x,cellModel.y) === "false" && i >= 0; i--) {
                    var cellModelTemp = board[cellModel.y][i];
                    if(isEmpty(cellModelTemp.x,cellModelTemp.y) === "true"){
                        break;
                    }
                    cellModel = cellModelTemp;
                } 
            } else {
               cellModel = board[firstCellComponent.x][firstCellComponent.x];
            }
        }
        if(isEmpty(cellModel.x,cellModel.y) === "true") {
            return firstCellComponent;
        }
        
        return cellModel;
    };
    
    var getWord = function(orientation, startCell) {
        var currCell = startCell;
        var word = "";
       
        if(orientation === "VERTICAL") {
            var i = startCell.y + 1;
            
            while("true" === "true") {
                if(isEmpty(currCell.x,currCell.y) === "false"){
                    word += currCell.value;
                } else {
                    break;
                }
                if(i < ROW) {
                    currCell = board[i][currCell.x];
                } else {
                    break;
                }
                i++;
            }
        }
        
        if(orientation === "HORIZONTAL") {
            var i = startCell.x + 1;
            
            while("true" === "true") {
                if(isEmpty(currCell.x,currCell.y) === "false"){
                    word += currCell.value;
                } else {
                    break;
                }
                if(i < COL) {
                    currCell = board[currCell.y][i];
                } else {
                    break;
                }
                i++;
            }
        }
        return word;
    };
    
    var getLettersOnBoard = function(){
        var letters = "";
        
        for(i=0; i < changedCellsList.length; i++) {
            letters += changedCellsList[i].value;
        }
        
        return letters;
    }
    
    var submitToServer = function(lettersStartPos, orientation, letters, wordStartPos, orientation, word) {
        
        data =  {
                    lettersStartPos: lettersStartPos,
                    orientation: orientation,
                    letters: letters,
                    wordStartPos: wordStartPos,
                    word: word
                };
        
//        var res = $http({method: 'post', url: 'makeWord', headers: {'Content-type': 'application/json'}});
        
        var req = {
                    method: 'POST',
                    url: 'makeWord',
                    headers: {
                        'Content-Type': 'application/json'
                    }, 
                    data: data,
        }
        
        // TODO - handle errors
        $http(req).success(function(data, status, headers, config) {
                console.log(data);
                if(data !== "")
                    Notifier.error(data);
        }).
        error(function(data, status, headers, config) {
               removeUncommitedCards();
               console.log("error submiting to server");
        });	
    }
    
    var onDropHandle = function(data,evt)
    {
        var value = PollPlayers.clientPlayer.cards[data.index].dragedValue;
        if (value == 'joker') {
            openJokerDialog(this.x,this.y);
        }
        else
        {
            this.value= value;
            this.dropable= "false";
            this.dragable = "true";
            changedCellsList.push(this);
        }
        console.log("row:" + this.x + "col:" + this.y + ": dropped data==> " + value);
    };
    for (var i = 0, max = 15; i < max; i++) {
        for (var j = 0, max = 15; j < max; j++) {
            board[i][j] = {value: eb[i][j], dropable: "true",dragable: "false", x:j,y:i, onDrop: onDropHandle};
        }
    }
    
    var removeUncommitedCards = function removeUncommitedCards()
    {
        var cell;
        for (var i=0 ; i < changedCellsList.length ; i++) {
            cell = changedCellsList[i];
            cell.value = eb[cell.x][cell.y];
            cell.dropable = "true";
        }
        changedCellsList.length = 0;
    };
    
    var dialog;
    var openJokerDialog = function openJokerDialog(x,y)
    {
        dialog = ngDialog.open({
            template: '<h2>Please choose a card</h2>' +
                    '<table class="jokers">' +
                    '<tr>' + 
                        '<td ng-repeat=\'card in cards1 track by $index\'>' +
                            '<img class="cell" ng-click="clicked(card)" ng-src=\'icons/{{card}}.png\'>' +
                        '</td>' + 
                    '</tr>' + 
                    '<tr>' + 
                        '<td ng-repeat=\'card in cards2 track by $index\'>' +
                            '<img class="cell" ng-click="clicked(card)" ng-src=\'icons/{{card}}.png\'>' +
                        '</td>' + 
                    '</tr>' + 
                    '<tr>' + 
                        '<td ng-repeat=\'card in cards3 track by $index\'>' +
                            '<img class="cell" ng-click="clicked(card)" ng-src=\'icons/{{card}}.png\'>' +
                        '</td>' + 
                    '</tr>' + 
                    '</table>'
            ,
            className: 'ngdialog-theme-default',
            plain: true,
            showClose: false,
            overlay: true,
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                    $scope.cards1 = ['A','B','C','D','E','F','G','H'];
                    $scope.cards2 = ['I','G','K','L','M','N','O','P'];
                    $scope.cards3 = ['Q','R','S','T','U','V','W','X','Y','Z'];
                    $scope.clicked = function clicked(card){
                        board[y][x].value = card;
                        board[y][x].dropable= "false";
                        board[y][x].dragable = "true";
                        changedCellsList.push(board[y][x]);
                        dialog.close();
                    };
            }]
        });

    };
    
    var clearChangedCellsList = function clearChangedCellsList() {
        changedCellsList.length = 0;        
    }
    
    return {
      replace: replace,
      board: board,
      changeCells: changeCells,
      removeUncommitedCards: removeUncommitedCards,
      submit: submit,
      clearChangedCellsList: clearChangedCellsList
    };
});
