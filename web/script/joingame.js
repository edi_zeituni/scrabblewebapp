var refreshRate = 5000; //miliseconds

function refreshWaitingGames(games) {
    //clear all current users
    $("#waitinglist").empty();
    $.each(games || [], function(index, game) {
            console.log("Adding user #" + index + ": " + game);
            //create a new <option> tag with a value in it and
            //appeand it to the #userslist (div with id=userslist) element
            $('<option>' + game + '</option>').appendTo($("#waitinglist"));
    });
}

function ajaxWaitingGames() {
    $.ajax({
        url: "getWaitingGames",
        success: function(games) {
            refreshWaitingGames(games);
        }
    });
}

function refreshActiveGames(games) {
    //clear all current users
    $("#activelist").empty();
    $.each(games || [], function(index, game) {
        console.log("Adding user #" + index + ": " + game);
        //create a new <option> tag with a value in it and
        //appeand it to the #userslist (div with id=userslist) element
        $('<option>' + game + '</option>').appendTo($("#activelist"));
    });
}

function ajaxActiveGames() {
    $.ajax({
        url: "getActiveGames",
        success: function(games) {
            refreshActiveGames(games);
        }
    });
}

function submitJoinGame() {
    var data = $('#joingameform').serializeArray();
    data.push({name: 'gamename', value: $( "#waitinglist option:selected" ).text()});
    
    $.ajax({
        data: data,
        url: this.action,
        timeout: 2000,
        type: "POST",
        error: function() {
            console.error("Failed to submit join game");
        },
        success: function(response) {
            console.log("SUCCESS REDIRECTING USER");    
            window.location.replace("waitforplayers.html");
        }
    });

//    $("#userstring").val("");
//     by default - we'll always return false so it doesn't redirect the user.
    return false;
}

$(function() { // onload...do
    ajaxWaitingGames();
    setInterval(ajaxWaitingGames, refreshRate);
    ajaxActiveGames();
    setInterval(ajaxActiveGames, refreshRate);
    
    $("#joingameform").submit(submitJoinGame); 
});