var app = angular.module('eventsManager', ['boardManager','cardsManager','playersManager']);
  

app.run(function(Poller) {});

app.factory('Poller', function($http, $timeout,Board,Cards,PollPlayers) {
  var lastEventID = 0;
  var winnerPlayerName;
  var isGameOver = false;
  var queue = [];
  var shouldLockUI = {status : "yes"};
  
  var processPlayerAction = function processPlayerAction(event){
        switch(event.playerAction){
            case "MAKE_WORD":
                var res = PollPlayers.updateScoreForPlayer(event.playerName, event.score);
                console.log("RES IS: " + res);
                if(res === "true") {
                    console.log("processPlayerAction!!!! AND TRUE");
                    Board.clearChangedCellsList();
                    Notifier.success('You gaind ' + event.score + ' points');
                } 
                if(res === "false") {
                    console.log("processPlayerAction!!!! AND FALSE");
                    Board.removeUncommitedCards();
                    Notifier.error('You lost ' + event.score + ' points');
                }
                break;
            case "PASS_TURN":
                //TODO - notify mainApp to animate on pass button
//                controller.notifyPassAction();
                break;
            case "REPLACE_LETTERS":
                if (event.playerName === PollPlayers.clientPlayer.name) {
                    PollPlayers.pollClientPlayerCards();
                }
                break;
            default:
                break;
        }
    };
    
  var poller = function() {
    $http.get('getGameEvents').then(function(r) {
        var events = r.data;
        if (events && events.length > 0) 
            lastEventID = events[events.length-1].id;
        
        if (events.length > 0) {
            console.log("last event id: " + lastEventID);
            console.log("new events: " + events.length);
            events.forEach(function(entry) {
               queue.push(entry);
            });
      }
      
        var event = queue.shift();
        if(event){
            switch (event.type){
            case "PROMPT_PLAYER_TO_TAKE_ACTION":
                if (event.playerName === PollPlayers.clientPlayer.name) {
                    Notifier.info('Your turn');
                }
                // nothing to do because button already unlocked from 'PLAYER_TURN' processing
                break;
            case "GAME_OVER":
                //TODO - the mainApp should listen to this property
                isGameOver = true;
              break;
            case "GAME_START":
                //we are inside the run loop, it means that the game has started and there is no need to take any action
                //for this event
                break;
            case "GAME_WINNER":
                winnerPlayerName = event.playerName;
                isGameOver = true;
                break;
            case "LETTERS_ON_BOARD":
                Board.changeCells(event.position , event.orientation, event.letters);
                break;
            case "PLAYER_ACTION":
                processPlayerAction(event);
                break;
            case "PLAYER_RESIGNED":
                PollPlayers.resignPlayer(event.playerName);
                Notifier.info("Player resigned:" + event.playerName);
                break;
            case "PLAYER_TURN":
                PollPlayers.currentPlayerHasChanged(event.playerName);
                break;
            default:
                break;
            }
        }
      $timeout(poller, 1000);
    });
    
  };
  poller();
  
  return {
    shouldLockUI: shouldLockUI,
    isGameOver: isGameOver,
    winnerPlayerName: winnerPlayerName
  };
});