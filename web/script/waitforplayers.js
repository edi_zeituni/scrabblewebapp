var refreshRate = 5000; //miliseconds

function ajaxGetGameDetails() {
    $.ajax({
        url: "getGameDetails",
        timeout: 2000,
        error: function() {
            console.error("Failed to get game details");
        },
        success: function(response) {
            console.log(response);
            var connected = response.computerizedPlayers + response.joinedHumanPlayers;
            var notConnected = response.humanPlayers + response.computerizedPlayers;
            var output = connected.toString() + " connected to game '" + response.name + "' out of " + notConnected.toString();
            $('#gamestatus').text(output);
            if(response.joinedHumanPlayers === response.humanPlayers) {
                window.location.replace("game.html");
            }
        }
    });
}

$(function() { // onload...do
    ajaxGetGameDetails();
    setInterval(ajaxGetGameDetails, refreshRate);
});

