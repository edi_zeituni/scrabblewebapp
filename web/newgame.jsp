<!DOCTYPE html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="scrabble.utils.*" %>
<html>
    <head>
        <title>Scrabble New Game Page</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <FORM id="newgameform" action="NewGame" method="POST">
                <P>
                    <LABEL>Player name: </LABEL>
                        <INPUT type="text" name="playername"><BR>
                    <LABEL>Computer players: </LABEL>
                        <INPUT type="text" name="computerplayers"><BR>
                    <LABEL>Human players: </LABEL>
                        <INPUT type="text" name="humanplayers"><BR>
                    <LABEL>Game name: </LABEL>
                        <INPUT type="text" name="gamename"><BR>
                    <INPUT class="btn" type="submit" value="Start Game"><INPUT class="btn" type="reset">
                </P>
            </FORM>
            <% String errorMessage = SessionUtils.getError(request);%>
            <% if (errorMessage != null) {%>
                <span class="label important"><%=errorMessage%></span>
            <% } %>
        </div>
    </body>
</html>
